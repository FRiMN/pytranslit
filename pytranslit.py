#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pytranslit.py
#
#  Copyright 2013-2014 Freezeman <freezemandix@yandex.ru>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
import sys

#reload(sys)
#sys.setdefaultencoding('utf8')

def setup_console(sys_enc="utf-8"):
    import codecs
    reload(sys)
    try:
        # для win32 вызываем системную библиотечную функцию
        if sys.platform.startswith("win"):
            import ctypes
            enc = "cp%d" % ctypes.windll.kernel32.GetOEMCP() #TODO: проверить на win64/python64
        else:
            # для Linux всё, кажется, есть и так
            enc = (sys.stdout.encoding if sys.stdout.isatty() else
                        sys.stderr.encoding if sys.stderr.isatty() else
                            sys.getfilesystemencoding() or sys_enc)

        # кодировка для sys
        sys.setdefaultencoding(sys_enc)

        # переопределяем стандартные потоки вывода, если они не перенаправлены
        if sys.stdout.isatty() and sys.stdout.encoding != enc:
            sys.stdout = codecs.getwriter(enc)(sys.stdout, 'replace')

        if sys.stderr.isatty() and sys.stderr.encoding != enc:
            sys.stderr = codecs.getwriter(enc)(sys.stderr, 'replace')

    except:
        pass # Ошибка? Всё равно какая - работаем по-старому...

#setup_console()

# Из книги В.В. Шахиджаняна «Соло на клавиатуре»
test_ = u'Здесь фабула объять не может всех эмоций — шепелявый скороход в юбке тащит горячий мёд.'

test_en = u'When in the course of human Events, it becomes necessary for one People to dissolve the Political Bands which have connected them with another, and to assume among the Powers of the Earth, the separate and equal Station to which the Laws of Nature and of Nature`s God entitle them, a decent Respect to the Opinions of Mankind requires that they should declare the causes which impel them to the Separation.'


def leet_strict(s):
    """ Leet strict version

    >>> leet_strict(test_)

    """
    Table = {
        u"а":"4",       u"a":"4",
        u"б":"b",       u"b":"6",
        u"в":"8",       u"c":"(",
        u"г":"r",       u"d":"[)",
        u"д":"d",       u"e":"3",
        u"е":"3",       u"f":"|=",
        u"ё":"3",       u"g":"[+",
        u"ж":r"}|{",    u"h":"|-|",
        u"з":r"'/_",    u"i":"1",
        u"и":r"|",      u"j":"_/",
        u"й":r"|",      u"k":"|<",
        u"к":r"|<",     u"l":"|",
        u"л":"/\\",     u"m":"/\\/\\",
        u"м":r"|\/|",   u"n":"|\\|",
        u"н":r"|-|",    u"o":"0",
        u"о":"0",       u"p":"|o",
        u"п":r"/7",     u"q":"O_",
        u"р":r"|>",     u"r":"|^",
        u"с":r"[",      u"s":"5",
        u"т":"7",       u"t":"7",
        u"у":r"'/",     u"u":"|_|",
        u"ф":"qp",      u"v":"\\/",
        u"х":"><",      u"w":"vv",
        u"ц":"||_",     u"x":"><",
        u"ч":"4",       u"y":"`/",
        u"ш":"LLI",     u"z":"2",
        u"щ":"LLL",
        u"ъ":"'b",
        u"ы":"b|",
        u"ь":"b",
        u"э":"э",
        u"ю":"10",
        u"я":"9",
    }
    pre(s)
    s = s.lower()       # большие и маленькие буквы не различаются
    ns = convert(s, Table)
    return ns


def leet_simple(s):
    """ Leet simple version """
    import re
    s = re.sub(r'\sAND\s', ' & ', s, flags=re.IGNORECASE)


def iso9(s, system='b'):
    """ ISO 9 (ГОСТ 7.79)
    full unambiguity (полная однозначность)

    >>> iso9(u'', system='a')
    u''
    >>> iso9(u'', system='b')
    u''
    >>> iso9(u'', system='c')
    Traceback (most recent call last):
      ...
    ValueError: Unknown function argument
    """
    if system == 'a':
        pass
    elif system == 'b':
        return iso9_b(s)
    else:
        raise ValueError("Unknown function argument")


def iso9_b(s):
    ''' ISO 9 system B simple
    (с использованием буквосочетаний)

    >>> iso9_b(test_)
    u'Zdes` fabula ob``yat` ne mozhet vsex e`moczij \u2014 shepelyavy`j skoroxod v yubke tashhit goryachij myod.'

    >>> iso9_b('not Unicode string')
    Traceback (most recent call last):
      ...
    TypeError: Only a Unicode string can be passed to the function
    '''
    Table = {
        u"а":"a",       u"А":"A",
        u"б":"b",       u"Б":"B",
        u"в":"v",       u"В":"V",
        u"г":"g",       u"Г":"G",
        u"д":"d",       u"Д":"D",
        u"е":"e",       u"Е":"E",
        u"ё":"yo",      u"Ё":"Yo",
        u"ж":"zh",      u"Ж":"Zh",
        u"з":"z",       u"З":"Z",
        u"и":"i",       u"И":"I",
        u"й":"j",       u"Й":"J",
        u"i":"i",       u"I":"I",
        u"к":"k",       u"К":"K",
        u"л":"l",       u"Л":"L",
        u"м":"m",       u"М":"M",
        u"н":"n",       u"Н":"N",
        u"о":"o",       u"О":"O",
        u"п":"p",       u"П":"P",
        u"р":"r",       u"Р":"R",
        u"с":"s",       u"С":"S",
        u"т":"t",       u"Т":"T",
        u"у":"u",       u"У":"U",
        u"ф":"f",       u"Ф":"F",
        u"х":"x",       u"Х":"X",
        u"ц":"cz",      u"Ц":"Cz",
        u"ч":"ch",      u"Ч":"Ch",
        u"ш":"sh",      u"Ш":"Sh",
        u"щ":"shh",     u"Щ":"Shh",
        u"ъ":"``",      u"Ъ":"``",
        u"ы":"y`",      u"Ы":"Y`",
        u"ь":"`",       u"Ь":"`",
        u"э":"e`",      u"Э":"E`",
        u"ю":"yu",      u"Ю":"Yu",
        u"я":"ya",      u"Я":"Ya",
        u"`":"'",       u"\u2116":"#",
        u"\u0463":"ye", u"\u0462":"Ye",
        u"\u0473":"fh", u"\u0472":"Fh",
        u"\u0475":"yh", u"\u0474":"Yh",
    }
    pre(s)
    ns = convert(s, Table)
    return ns


def bgn(s):
    """ BGN/PCGN
    `Source: <http://transliteration.eki.ee/pdf/Russian.pdf>`_
    `Source: <http://en.wikipedia.org/wiki/BGN/PCGN_romanization_of_Russian>`_

    >>> print bgn(test_).encode("utf8")
    Zdes` fabula ob``yat` ne mozhet vsekh emotsiy — shepelyavyy skorokhod v yubke tashchit goryachiy mёd.

    >>> print bgn('Ёлкин, Елец, Йена, Генриетта'.decode("utf8")).encode("utf8")
    Yёlkin, Yelets, Jyena, Genriyetta

    >>> bgn('not Unicode string')
    Traceback (most recent call last):
      ...
    TypeError: Only a Unicode string can be passed to the function
    """
    import re
    Table = {
        u"а":"a",       u"А":"A",
        u"б":"b",       u"Б":"B",
        u"в":"v",       u"В":"V",
        u"г":"g",       u"Г":"G",
        u"д":"d",       u"Д":"D",
        u"е":"e",       u"Е":"Ye",
        u"ё":u"ё",      u"Ё":u"Yё",
        u"ж":"zh",      u"Ж":"Zh",
        u"з":"z",       u"З":"Z",
        u"и":"i",       u"И":"I",
        u"й":"y",       u"Й":"J",
        u"к":"k",       u"К":"K",
        u"л":"l",       u"Л":"L",
        u"м":"m",       u"М":"M",
        u"н":"n",       u"Н":"N",
        u"о":"o",       u"О":"O",
        u"п":"p",       u"П":"P",
        u"р":"r",       u"Р":"R",
        u"с":"s",       u"С":"S",
        u"т":"t",       u"Т":"T",
        u"у":"u",       u"У":"U",
        u"ф":"f",       u"Ф":"F",
        u"х":"kh",      u"Х":"Kh",
        u"ц":"ts",      u"Ц":"Ts",
        u"ч":"ch",      u"Ч":"Ch",
        u"ш":"sh",      u"Ш":"Sh",
        u"щ":"shch",    u"Щ":"Shch",
        u"ъ":'``',      u"Ъ":'``',
        u"ы":"y",       u"Ы":"Y",
        u"ь":"`",       u"Ь":"`",
        u"э":"e",       u"Э":"E",
        u"ю":"yu",      u"Ю":"Yu",
        u"я":"ya",      u"Я":"Ya",
        u"\u2116":"#",
    }
    pre(s)

    s = re.sub(u'(?<=[йЙъьаАеЕёЁиИоОуУыЫэЭюЮяЯ])е', u'ye', s)
    s = re.sub(u'(?<=[йЙъьаАеЕёЁиИоОуУыЫэЭюЮяЯ])ё', u'yё', s)

    ns = convert(s, Table)
    return ns


def telegramm(s):
    ''' Международные телеграммы

    >>> telegramm(test_)
    u'Zdes fabula obiat ne mojet vseh emocii \u2014 shepeliavyi skorohod v iubke tascit goriachii med.'

    >>> telegramm('not Unicode string')
    Traceback (most recent call last):
      ...
    TypeError: Only a Unicode string can be passed to the function
    '''
    Table = {
        u"а":"a",       u"А":"A",
        u"б":"b",       u"Б":"B",
        u"в":"v",       u"В":"V",
        u"г":"g",       u"Г":"G",
        u"д":"d",       u"Д":"D",
        u"е":"e",       u"Е":"E",
        u"ё":"e",       u"Ё":"E",
        u"ж":"j",       u"Ж":"J",
        u"з":"z",       u"З":"Z",
        u"и":"i",       u"И":"I",
        u"й":"i",       u"Й":"I",
        u"к":"k",       u"К":"K",
        u"л":"l",       u"Л":"L",
        u"м":"m",       u"М":"M",
        u"н":"n",       u"Н":"N",
        u"о":"o",       u"О":"O",
        u"п":"p",       u"П":"P",
        u"р":"r",       u"Р":"R",
        u"с":"s",       u"С":"S",
        u"т":"t",       u"Т":"T",
        u"у":"u",       u"У":"U",
        u"ф":"f",       u"Ф":"F",
        u"х":"h",       u"Х":"H",
        u"ц":"c",       u"Ц":"C",
        u"ч":"ch",      u"Ч":"Ch",
        u"ш":"sh",      u"Ш":"Sh",
        u"щ":"sc",      u"Щ":"Sc",
        u"ъ":"",        u"Ъ":"",
        u"ы":"y",       u"Ы":"Y",
        u"ь":"",        u"Ь":"",
        u"э":"e",       u"Э":"E",
        u"ю":"iu",      u"Ю":"Iu",
        u"я":"ia",      u"Я":"Ia",
    }
    pre(s)
    ns = convert(s, Table)
    return ns


def yakovlev(s):
    """ Советский проект латинизации 1929-1930 годов под руководством Н.Ф. Яковлева

    >>> yakovlev("этот, еда, подъезд, ёлка, подъём, яма, изъян, юг, рай, зной, улей, мел, мал, мял, лук, люк, мол, мёл, мель, кон, конь, все, всё, чёрный".decode("utf8"))
    u'etot, jeda, podjezd, jolka, podjom, jama, izjan, jug, raj, znoj, ulej, mel, mal, mjal, luk, ljuk, mol, mjol, melj, kon, konj, vse, vsjo, cornyj'
    """
    import re

    Table = {
        u"а":"a",       u"А":"A",
        u"б":"b",       u"Б":"B",
        u"в":"v",       u"В":"V",
        u"г":"g",       u"Г":"G",
        u"д":"d",       u"Д":"D",
        u"е":"e",       u"Е":"E",
        u"ё":"jo",      u"Ё":"Jo",
        u"ж":r"z",      u"Ж":"Z",
        u"з":r"z",      u"З":"Z",
        u"и":r"i",      u"И":"I",
        u"й":r"j",      u"Й":"J",
        u"к":r"k",      u"К":"K",
        u"л":"l",       u"Л":"L",
        u"м":r"m",      u"М":"M",
        u"н":r"n",      u"Н":"N",
        u"о":"o",       u"О":"O",
        u"п":r"p",      u"П":"P",
        u"р":r"r",      u"Р":"R",
        u"с":r"s",      u"С":"S",
        u"т":"t",       u"Т":"T",
        u"у":r"u",      u"У":"U",
        u"ф":"f",       u"Ф":"F",
        u"х":"x",       u"Х":"X",
        u"ц":"c",       u"Ц":"C",
        u"ч":"c",       u"Ч":"C",
        u"ш":"s",       u"Ш":"S",
        u"щ":"sc",      u"Щ":"Sc",
        u"ъ":"",        u"Ъ":"",
        u"ы":"y",       u"Ы":"Y",
        u"ь":"j",       u"Ь":"J",
        u"э":"e",       u"Э":"E",
        u"ю":"ju",      u"Ю":"Ju",
        u"я":"ja",      u"Я":"Ja",
    }
    pre(s)
    #s = s.lower()       # большие и маленькие буквы не различаются

    s = re.sub(u'(?<=[йЙъьаАеЕёЁиИоОуУыЫэЭюЮяЯ])е', u'je', s)
    s = re.sub(u'(?<=[йЙъьаАеЕёЁиИоОуУыЫэЭюЮяЯ])ё', u'jo', s)
    s = re.sub(u'(?<=[йЙъьаАеЕёЁиИоОуУыЫэЭюЮяЯ])ю', u'ju', s)
    s = re.sub(u'(?<=[йЙъьаАеЕёЁиИоОуУыЫэЭюЮяЯ])я', u'ja', s)

    s = re.sub(u'(?<!\S)е(?=\S+)', u'je', s)
    s = re.sub(u'(?<!\S)ё(?=\S+)', u'jo', s)
    s = re.sub(u'(?<!\S)ю(?=\S+)', u'ju', s)
    s = re.sub(u'(?<!\S)я(?=\S+)', u'ja', s)

    ns = convert(s, Table)
    return ns


def qwerty(s):
    """ Восстановление раскладки

    >>> sys.stdout.write( qwerty('йцукен'.decode("utf8")) )
    qwerty

    >>> qwerty('Црут шт еру сщгкыу ща ргьфт Умутеыб ше иусщьуы тусуыыфкн ащк щту Зущзду ещ вшыыщдму еру Зщдшешсфд Ифтвы цршср рфму сщттусеув еруь цшер фтщерукб фтв ещ фыыгьу фьщтп еру Зщцукы ща еру Уфкерб еру ыузфкфеу фтв уйгфд Ыефешщт ещ цршср еру Дфцы ща Тфегку фтв ща Тфегкуёы Пщв утешеду еруьб ф вусуте Куызусе ещ еру Щзштшщты ща Ьфтлштв куйгшкуы ерфе ерун ырщгдв вусдфку еру сфгыуы цршср шьзуд еруь ещ еру Ыузфкфешщтю'.decode("utf8"))
    u'When in the course of human Events, it becomes necessary for one People to dissolve the Political Bands which have connected them with another, and to assume among the Powers of the Earth, the separate and equal Station to which the Laws of Nature and of Nature`s God entitle them, a decent Respect to the Opinions of Mankind requires that they should declare the causes which impel them to the Separation.'
    """

    Table_ru2en = {
        u"й":"q",       u"Й":"Q",
        u"ц":"w",       u"Ц":"W",
        u"у":"e",       u"У":"E",
        u"к":"r",       u"К":"R",
        u"е":"t",       u"Е":"T",
        u"н":"y",       u"Н":"Y",
        u"г":"u",       u"Г":"U",
        u"ш":"i",       u"Ш":"I",
        u"щ":"o",       u"Щ":"O",
        u"з":"p",       u"З":"P",
        u"х":"[",       u"Х":"{",
        u"ъ":"]",       u"Ъ":"}",
        u"\\":"\\",     u"/":"|",
        u"ф":"a",       u"Ф":"A",
        u"ы":"s",       u"Ы":"S",
        u"в":"d",       u"В":"D",
        u"а":"f",       u"А":"F",
        u"п":"g",       u"П":"G",
        u"р":"h",       u"Р":"H",
        u"о":"j",       u"О":"J",
        u"л":"k",       u"Л":"K",
        u"д":"l",       u"Д":"L",
        u"ж":";",       u"Ж":";",
        u"э":"'",       u"Э":"'",
        u"я":"z",       u"Я":"Z",
        u"ч":"x",       u"Ч":"X",
        u"с":"c",       u"С":"C",
        u"м":"v",       u"М":"V",
        u"и":"b",       u"И":"B",
        u"т":"n",       u"Т":"N",
        u"ь":"m",       u"Ь":"M",
        u"б":",",       u"Б":"<",
        u"ю":".",       u"Ю":">",
        u".":"/",       u",":"?",
        u"ё":"`",       u"Ё":"~",
        u"\"":"@",      u"№":"#",
        u";":"$",       u":":"^",
        u"?":"&",
    }
    pre(s)
    #s = s.lower()       # большие и маленькие буквы не различаются

    ns = convert(s, Table_ru2en)
    return ns


def pre(s):
    """ Предварительная проверка """
    if not isinstance( s, unicode ):
        raise TypeError("Only a Unicode string can be passed to the function")


def convert(s, Table):
    """ Общая функция конвертирования """
    ns = u''
    for c in s:
        if c in Table:
            nc = Table[c]
            ns = ns + nc
        else:
            ns = ns + c
            #ns = ns + '_'
    return ns


if __name__ == '__main__':
    testwords = [
                u'Анатолий Петрович`с Фуфловский',
                u'Пулемёт ППШ',
                u'Цембаларём',
                u'Ваш id №221',
                u'мѣсто',
                u'Ѳетюкъ слово обидное для мужчины, происходитъ отъ Ѳ, буквы, почитаемой нѣкоторыми неприличною',
                u'Паровоз Ѵ',
                u'Ввиду того, что Вы, Ваша Светлость, принадлежа к числу вельмож,  столь склонных поощрять изящные искусства, оказываете радушный  и  почётный  прием всякого рода книгам, наипаче же таким, которые  по  своему  благородству  не унижаются до  своекорыстного  угождения  черни,  положил  я  выдать  в  свет Хитроумного идальго Дон Кихота Ламанчского под защитой  достославного  имени Вашей Светлости и ныне,  с  тою  почтительностью,  какую  внушает  мне  Ваше величие, молю Вас принять его под  милостивое  своё  покровительство,  дабы, хотя  и  лишённый  драгоценных  украшений  изящества  и   учёности,   обычно составляющих   убранство   произведений,   выходящих   из-под   пера   людей просвещённых, дерзнул он под сенью Вашей Светлости бесстрашно  предстать  на суд тех, кто, выходя за пределы собственного невежества,  имеет  обыкновение при разборе чужих трудов выносить не столько справедливый,  сколько  суровый приговор, - Вы же, Ваша Светлость, вперив очи мудрости своей  в  мои  благие намерения, надеюсь, не отвергнете столь  слабого  изъявления  нижайшей  моей преданности.',
                u'Flying Monster',
                u'P-47D Thunderbold',
    ]



    import doctest
    doctest.testmod()
    #print iso9(u'фыва', 'u')

    #for test in testwords:
        #print iso9_b(test)

    #i = 0
    #while i < 500:
        #for test in testwords:
            #iso9_b(test)
        #i=i+1

